#Introduction
This project consists of :
* Spring boot 2
* Spring batch
* Json Web Token (Jwt)
* H2 in-memory DB
* JPA

# Design pattern
* Singleton
* Model View Controller (MVC)

# Getting Started
### Required tools

* JDK 11
* Maven
* IntelliJ IDEA
* REST testing tool. i.e. Insomia/Postman

1. Import the customer-data project, 
2. Once import successfully & all the dependencies are downloaded, you will see the modules as listed below.

### Modules
* customer-data-model    
  * Entity module for the project
  
* customer-data-service  
  * Service layer & DAO layer for the project
  
* customer-data-loader
  * RAW data loading batch job
  
* customer-data-webapi
  * REST API for retrieve and update the customer data
  
### Class Diagram
![alt text](<./customer-data-class-diagram.png>)
  
### Steps
1. Open the terminal, navigate to "customer-data". execute command below
   1. ./mvnw clean verify package install
2. Start up the web services by executing the command below 
   1. ./mvnw -pl customer-data-webapi spring-boot:run
3. Once started up successfully, you should be able to access to the link below.
   1. REST API Doc - http://localhost:8080/swagger-ui.html
   2. h2 DB console - http://localhost:8080/h2
      1. JDBC URL - jdbc:h2:mem:testdb
4. To load the customer data. Open another terminal, navigate to "customer-data" execute command below
   1. ./mvnw -pl customer-data-loader spring-boot:run
5. Once executed, you should be able to retrieve the data in h2 DB console.
6. You can use those REST testing tools to obtain the JWT token. Use credential below and post to the URL, http://localhost:8080/authenticate
   1. restuser/password
7. For the REST API, please check out the swagger doc.And each API request shall include the JWT token as Authorization header.

Note: All the data will be flushed once the application closed.