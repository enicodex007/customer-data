DROP TABLE IF EXISTS tbl_customer_transaction;

CREATE TABLE tbl_customer_transaction (
  id BIGINT AUTO_INCREMENT  PRIMARY KEY,
  account_no BIGINT NOT NULL,
  trx_amount decimal NOT NULL,
  trx_date timestamp not null,
  description VARCHAR(250) DEFAULT NULL,
  cust_id BIGINT not null,
  version BIGINT not null default 0,
  update_time timestamp not null
);