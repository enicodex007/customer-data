package com.maybank.interview;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@EnableTransactionManagement
@ComponentScan({"com.maybank.interview"})
public class CustomerDataWebApp {

    public static void main(String[] args){
        SpringApplication.run(CustomerDataWebApp.class, args);
    }
}
