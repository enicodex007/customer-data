package com.maybank.interview.controller;

import com.maybank.interview.model.CustomerTransaction;
import com.maybank.interview.service.CustomerTransactionService;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.annotations.NotFound;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.Optional;


@Slf4j
@RestController
@RequestMapping("/customer/transaction")
public class CustomerTransactionController {

    @Autowired
    private CustomerTransactionService customerTransactionService;

    @ApiOperation(value = "Retrieve all customer transaction", notes = "GET ALL Customer transaction")
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public List<CustomerTransaction> getAllCustomerTrx(
            @RequestParam(defaultValue = "0", required = false) Integer pageNo,
            @RequestParam(defaultValue = "20", required = false) Integer pageSize,
            @RequestParam(defaultValue = "", required = false) String accountNo,
            @RequestParam(defaultValue = "", required = false) String custId,
            @RequestParam(defaultValue = "", required = false) String description){

        return customerTransactionService.findAll(pageNo, pageSize, description, accountNo, custId);
    }

    @ApiOperation(value = "UPDATE Customer trx Description", notes = "UPDATE Customer trx Description")
    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity<?> updateCustomerTrxDescription(
            @PathVariable(value = "id", required = true) long id,
            @RequestBody String description){
        Optional<CustomerTransaction> optCurrentTrx = customerTransactionService.findById(id);
        if(!optCurrentTrx.isPresent()){
            String errMsg = "id-"+id+"not found!";
            log.error("{}", errMsg);
            return new ResponseEntity<String>(errMsg, HttpStatus.NOT_FOUND);
        }

        CustomerTransaction current = optCurrentTrx.get();
        Optional<CustomerTransaction> updated = customerTransactionService.updateDescription(current, description, current.getVersion()+1l, new Date());

        if(!updated.isPresent()){
            String errMsg = "id-"+id+"update failure!";
            log.error("{}", errMsg);
            return new ResponseEntity<String>(errMsg, HttpStatus.MOVED_PERMANENTLY);
        }

        return new ResponseEntity<CustomerTransaction>(updated.get(), HttpStatus.OK);
    }

}
