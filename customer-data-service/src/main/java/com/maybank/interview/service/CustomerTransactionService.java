package com.maybank.interview.service;

import com.maybank.interview.model.CustomerTransaction;
import com.maybank.interview.repo.CustomerTransactionRepository;
import com.maybank.interview.repo.SearchCriteria;
import com.maybank.interview.repo.SearchOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class CustomerTransactionService {

    @Autowired
    private CustomerTransactionRepository customerTransactionRepository;

    @Transactional(readOnly = true)
    public List<CustomerTransaction> findAll(int pageNo, int pageSize) {
        return findAll(pageNo, pageSize, "", "", "");
    }

    @Transactional(readOnly = true)
    public List<CustomerTransaction> findAll(int pageNo, int pageSize, String description, String accountNo, String custId) {
        Pageable pageable = PageRequest.of(pageNo, pageSize);

        Page<CustomerTransaction> pagedResult;
        CustomerTransactionSpec customerTransactionSpec = null;
        if(!description.isEmpty() || !accountNo.isEmpty() || !custId.isEmpty()) {
            customerTransactionSpec  = new CustomerTransactionSpec();
        }

        if(!description.isEmpty()) {
            customerTransactionSpec.add(new SearchCriteria("description", description, SearchOperation.MATCH));
        }
        if(!accountNo.isEmpty()) {
            customerTransactionSpec.add(new SearchCriteria("accountNo", accountNo, SearchOperation.EQUAL));
        }
        if(!custId.isEmpty()) {
            customerTransactionSpec.add(new SearchCriteria("custId", custId, SearchOperation.EQUAL));
        }

        if(customerTransactionSpec!=null) {
            pagedResult = customerTransactionRepository.findAll(customerTransactionSpec, pageable);
        }else {
            pagedResult = customerTransactionRepository.findAll(pageable);
        }

        if(pagedResult.hasContent()) {
            return pagedResult.getContent();
        } else {
            return new ArrayList<CustomerTransaction>();
        }
    }

    @Transactional(readOnly = true)
    public Optional<CustomerTransaction> findById(Long id) {
       return customerTransactionRepository.findById(id);
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public CustomerTransaction save(CustomerTransaction customerTransaction){
        return customerTransactionRepository.save(customerTransaction);
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public Optional<CustomerTransaction> updateDescription(CustomerTransaction current, String description, Long newVersion, Date updateTime){
        int rowAffected = customerTransactionRepository.updateDescription(current.getId(), current.getVersion(),
                description, newVersion, updateTime);

        if(rowAffected>0){
            return customerTransactionRepository.findById(current.getId());
        }else{
            return Optional.empty();
        }
    }


}
