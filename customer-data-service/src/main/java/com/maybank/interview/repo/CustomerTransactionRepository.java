package com.maybank.interview.repo;

import com.maybank.interview.model.CustomerTransaction;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import org.springframework.data.repository.query.Param;

import java.util.Date;

public interface CustomerTransactionRepository extends PagingAndSortingRepository<CustomerTransaction, Long>,
        JpaSpecificationExecutor<CustomerTransaction>{

    @Modifying(clearAutomatically = true)
    @Query("UPDATE tbl_customer_transaction SET description = :description, version = :newVersion, update_time = :updateTime  WHERE id = :id and version = :currentVersion ")
    int updateDescription(@Param("id") long id, @Param("currentVersion") Long currentVersion,
                          @Param("description") String description,
                          @Param("newVersion") Long newVersion,
                          @Param("updateTime") Date updateTime);

}
