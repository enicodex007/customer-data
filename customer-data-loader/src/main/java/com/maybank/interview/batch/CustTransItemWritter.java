package com.maybank.interview.batch;

import com.maybank.interview.model.CustomerTransaction;
import com.maybank.interview.service.CustomerTransactionService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;

import java.util.List;

@Slf4j
public class CustTransItemWritter implements ItemWriter<CustomerTransaction> {

    @Autowired
    private CustomerTransactionService customerTransactionService;

    @Override
    public void write(List<? extends CustomerTransaction> list) throws Exception {
        log.info("total records-{}", list.size());
        if(list.isEmpty()){
            return;
        }
        list.forEach(trx ->{
            customerTransactionService.save(trx);
        });
    }
}
