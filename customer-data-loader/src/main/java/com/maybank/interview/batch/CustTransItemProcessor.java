package com.maybank.interview.batch;

import com.maybank.interview.model.CustomerTransaction;
import org.springframework.batch.item.ItemProcessor;

public class CustTransItemProcessor implements ItemProcessor<CustomerTransaction, CustomerTransaction> {

    @Override
    public CustomerTransaction process(CustomerTransaction customerTransaction) throws Exception {
        return customerTransaction;
    }
}
