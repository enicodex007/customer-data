package com.maybank.interview.batch;

import com.maybank.interview.model.CustomerTransaction;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;
import org.springframework.boot.context.properties.bind.BindException;

import java.sql.Date;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;

@Slf4j
public class CustTransRecordFieldSetMapper implements FieldSetMapper<CustomerTransaction> {

    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    public CustomerTransaction mapFieldSet(FieldSet fieldSet) throws BindException {

        int index = 0;
        CustomerTransaction transaction = new CustomerTransaction();
        transaction.setAccountNo(fieldSet.readLong(index++));
        transaction.setTrxAmount(fieldSet.readBigDecimal(index++));
        transaction.setDescription(fieldSet.readString(index++));

        String trxDateTime = fieldSet.readString(index++).concat(" ").concat(fieldSet.readString(index++));
        transaction.setTrxDate(Date.from(LocalDateTime.parse(trxDateTime, formatter).toInstant(ZoneOffset.UTC)));
        transaction.setCustId(fieldSet.readLong(index++));
        transaction.setUpdateTime(new java.util.Date());
        transaction.setVersion(0l);
        log.info(transaction.toString());
        return transaction;
    }
}
