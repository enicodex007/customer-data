package com.maybank.interview.batch;

import com.maybank.interview.model.CustomerTransaction;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.item.*;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;

@Slf4j
@Configuration
@EnableBatchProcessing
public class CustTransBatchConfig {

    @Autowired
    public JobBuilderFactory jobBuilderFactory;

    @Autowired
    public StepBuilderFactory stepBuilderFactory;

    @Value("${classpath:dataSource.txt}")
    private Resource sourceFile;

    @Bean
    public Job customerTransactionJob() {
        return jobBuilderFactory.get("customerTransactionJob").start(customerTransactionStep()).build();
    }

    @Bean
    public Step customerTransactionStep() {
        return stepBuilderFactory.get("customerTransactionStep").<CustomerTransaction, CustomerTransaction> chunk(10)
                .reader(itemReader()).processor(itemProcessor()).writer(custTransItemWritter()).build();
    }

    @Bean
    public ItemReader<CustomerTransaction> itemReader() throws UnexpectedInputException, ParseException {
        FlatFileItemReader<CustomerTransaction> reader = new FlatFileItemReader<>();
        DelimitedLineTokenizer tokenizer = new DelimitedLineTokenizer("|");
        String[] tokens = { "ACCOUNT_NUMBER", "TRX_AMOUNT", "DESCRIPTION", "TRX_DATE", "TRX_TIME", "CUSTOMER_ID" };
        tokenizer.setNames(tokens);
        reader.setResource(sourceFile);
        DefaultLineMapper<CustomerTransaction> lineMapper = new DefaultLineMapper<>();
        lineMapper.setLineTokenizer(tokenizer);
        lineMapper.setFieldSetMapper(new CustTransRecordFieldSetMapper());
        reader.setLineMapper(lineMapper);
        reader.setLinesToSkip(1);
        return reader;
    }

    @Bean
    public ItemProcessor<CustomerTransaction, CustomerTransaction> itemProcessor() {
        return new CustTransItemProcessor();
    }

    @Bean
    public CustTransItemWritter custTransItemWritter(){
        return new CustTransItemWritter();
    }

}
