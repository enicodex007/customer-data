package com.maybank.interview.model;

import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Data
@Entity(name = "tbl_customer_transaction")
public class CustomerTransaction {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name="id")
    private Long id;
    @Column(name = "account_no")
    private long accountNo;
    @Column(name = "trx_amount")
    private BigDecimal trxAmount;
    @Column(name = "description")
    private String description;
    @Column(name = "trx_date")
    private Date trxDate;
    @Column(name = "cust_id")
    private long custId;
    @Column(name = "version")
    private long version;
    @Column(name = "update_time")
    private Date updateTime;
}
